import React from 'react';
import logo from '../logo.svg';
import '../App.css';

function Home() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
        Home Page
        </p>
        {/* <button onClick={() => history.push('http://localhost:3001/')}>Button</button> */}
        <button>Button</button>
      </header>
    </div>
  );
}

export default Home;
