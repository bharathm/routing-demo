import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import App from '../App';
import Home from './Home'
import onBoarding from './onBoarding'
import transmission from './transmission'

// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"
const Main = () => (
    
  <main>
      <Router >
    <Switch>
      <Route path='/' component={Home}/>
      <Route path='/on-boarding' component={onBoarding}/>
      <Route path='/transmission' component={transmission}/>
    </Switch>
    </Router>
  </main>
)

export default Main